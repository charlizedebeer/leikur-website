import React from 'react'
import HeroMain from "../components/hero/Hero";
import ContentFullPage from "../components/ui-components/FullPageLayout"
const Home = () => {
    return(
        <ContentFullPage>
            <HeroMain/>
        </ContentFullPage>
    )
}

export default Home