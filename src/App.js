import React from "react";
import {Styles, CSSReset} from "./components/ui-components/Styles";
import Header from "./components/header/Header";
import HeroMain from "./components/hero/Hero"
import Features from "./components/icon-features/features";
import ContentFullPage from "./components/ui-components/FullPageLayout"
import CardsBanner from "./components/content-banners/card/CardsBanner"
import AccountsBanner from "./components/content-banners/accounts/AccountsBanner"
import WalletBanner from "./components/content-banners/wallet/WalletsBanner"
import AboutBanner from "./components/content-banners/about/AboutBanner";
import  Footer from "./components/footer/footer"
import Helmet from "react-helmet"

function App() {

    return(
<div>
    <Helmet>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </Helmet>
            <Styles.Wrapper>

                <CSSReset/>
                <ContentFullPage>
                    <Header />
                        <HeroMain/>
                        <Features/>
                    <AccountsBanner/>
                    <CardsBanner/>
                        <WalletBanner/>
                    <AboutBanner/>
                    <Footer/>
                </ContentFullPage>
            </Styles.Wrapper>
</div>

)
}

export default App
