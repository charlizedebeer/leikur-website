import React from 'react'
import styled from "styled-components"
const FeaturesWrapper = styled.div`
  display: inline-block;
  padding: 50px 10vw;
  min-height: 100%;
  width: 100%;
  min-width: 100vw;
  
`
const FeauturesTitle = styled.div`
  padding: 25px 15px;
  margin-bottom: 25px;
text-align: center;
  font-size: calc(2rem + 1vw);
  font-weight: bold;
  @media only screen and (max-width: 500px){
    font-size: calc(1.75rem + 1vw);
    padding: 25px 10px;


  }
`

const FeaturesGrid = styled.section`

  display: inline-flex;
  flex-wrap: wrap;
  padding: 1rem;
  @media only screen and (max-width: 500px){
  display: flex;
    flex: 0;
  }
  
  
`


import MoveIcon from "./Move .png"
import ConvertIcon from "./Convert.png"
import SendIcon from "./Send.png"


const Feature = styled.div`
  display: grid;
  flex: 33.33%;
  grid-template-columns: 1fr;
  grid-template-rows: repeat(2, auto-fit(20px, 1fr));
  grid-template-areas:
  "Icon"
    "Heading"
    "Description";
  justify-content: space-between;
  justify-items: center;
  align-items: center;
  padding: 2rem;`


const Icon = styled.div`
  justify-self: center;
  grid-area: Icon;
  min-width: 100px;
  min-height: 100px;
  align-content: center;
  padding: 10px;
  background-position: center center;
  background-repeat: no-repeat;
  background-size: contain;`
const MoveIcn = styled(Icon)`
background-image: url(${MoveIcon});
`
const ConvertIcn = styled(Icon)`
background-image: url(${ConvertIcon});
`

const SendIcn = styled(Icon)`
background-image: url(${SendIcon});
`
const Heading = styled.div`
  justify-self: center;
  font-weight: bold;
  padding:2rem;
  text-align: center;
  font-size: clamp(1rem, 10vw, 1.75rem);
  margin-top: 2rem;
  grid-area: Heading;
`
const Description = styled.div`
  justify-self: center;
  font-size: clamp(1rem, 10vw, 1.25rem); 
  text-align: center;
  width: fit-content ;
  grid-area: Description;
`

const Features = () => {
    return(
<FeaturesWrapper>
    <FeauturesTitle>
        A tailor-made banking service
    </FeauturesTitle>
    <FeaturesGrid>
        <Feature>
                <MoveIcn/>
                <Heading>
                    Move
                </Heading>
                <Description>
                    Addressing a global market of
                    users in need of smooth,
                    international payments and
                    banking solutions.
                </Description>
        </Feature>

        <Feature>
                <ConvertIcn/>
                <Heading>
                    Convert
                </Heading>
                <Description>
                    Providing tailor-made services
                    directed specifically towards
                    fulfilling the needs of your industry
                    - and your clients.
                </Description>

        </Feature>


            <Feature>
                <SendIcn/>
                <Heading>
                    Send
                </Heading>
                <Description>
                    Enhancing your offering and
                    branding towards your clients by
                    facilitating bank services, card
                    issuing and digital wallets.
                </Description>
            </Feature>

    </FeaturesGrid>
</FeaturesWrapper>
    )
}

export default Features