import React from 'react'
import styled from "styled-components"
import CardsBg from "./cards-bg.png"
import CardGraphic from "./cards-graphic.png"
import LayoutMargin from "../layout.js"
import Button from "../../ui-components/Button"



const Container = styled.div
    `
      overflow: hidden;
      width: 100%;
      height: 90vh;
      padding: 20vh 10vw;
      display: inline-block;
      background-image:  url("${CardsBg}");
      background-color: rgba(147, 139, 100, 1);
      background-size: cover;
      background-repeat:  no-repeat ;
      background-position: bottom;
      background-blend-mode: multiply;
      @media screen and (max-width: 1024px) {
        padding: 10vh 5vw;

      }

      @media screen and (max-width: 900px){
        padding: 2vh 5vw;
        height: fit-content;
        display: inline-block;
        flex-direction: row;
    `


    const Content = styled.div`
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      align-content: center;
      justify-content: space-between;
      @media screen and (max-width: 900px) {
        flex-direction: column;
      }
    `


export const ContentLeft = styled.div`
  flex: 0 1 45%;
  height: 50vh;
  display: grid;
  row-gap: 5vh;
  grid-template-rows: 1fr minmax(10vh, 25vh) minmax(20vh,35vh) 1fr;
  grid-template-areas: 
    "."
    "title"
    "description"
    ".";
  align-content: space-around;
  align-items: center;
  @media screen and (max-width: 900px) {
    max-width: 100%;
    height: fit-content;
    margin-top: -10vh;
  }
`



export const BannerPoints  = styled.ul`
 list-style: square;
  color: white;
  display: inline-block;
  align-self: center;
  grid-area: point;
  font-size: calc(8px + 1vh);
  @media screen and (max-width: 1024px) {
    padding: 0 0 2vh 5vw;
    margin-top: -2vh;
    max-width: 45ch;
  }
  @media screen and (max-width: 900px) {
    max-width: 100%;
    margin: 0 10px;
  }`


export const Bold = styled.span`
font-weight: bold;
 
`
export const BannerTitle  = styled.div`
  color: #fff;
  font-size: calc(1rem + 1vw);
  align-self: end;
  grid-area: title;
  display: inline-block;
  @media screen and (max-width: 1024px) {
    margin: 2vh 0;
  }
  @media screen and (max-width: 900px) {
    padding: 5vh 0 2.5vh 0;
  }
`
export const BannerKeyInformation = styled.div `
  font-size: calc(12px + 1vh);
  color: #fff;
  align-self: start;
  grid-area: description;
  display: inline-block;
  `





export const ContentRight = styled.div`
  flex: 0 1 45%;
  float: right;
  align-content: space-around;
  align-items: center;
  @media screen and (max-width: 1024px) {
    height: fit-content;
    min-width: 40vw;
    align-self: center;
    align-content: center;

    float: right;

  }
  @media screen and (max-width: 900px) {
    max-width: 100%;
    height: fit-content;

    flex: 100%
  }
`
export const Graphic = styled.img`
  width: 25rem;
  margin-top: -5rem;
  align-self: center;
  height: auto;
  grid-area: graphic;
  aspect-ratio: 1017/870;
  position: relative;
  @media screen and (max-width: 1024px) {
    width: 35vw;
    margin: 2vh 0 0 0;
    height: auto;
    float: right;
  }
  @media screen and (max-width: 900px) {
    width: 80vw;
    margin-top: -8rem;
  }
`





const CardBtn = styled(Button)`
  background-color: rgba(147, 139, 100, 1);
  padding: 10px 15px;
  display: inline-block;
  margin: 5vh 0;
  grid-area: button;
  @media screen and (max-width: 1024px) {
    margin: 1vh 4vw ;
  }
  @media screen and (max-width: 900px) {
    margin-bottom: 10vh;
  }

`





const CardsBanner = () => {
    return(
        <LayoutMargin>
        <Container>
            <Content>
            <ContentLeft>
                <BannerTitle>
                    <Bold> Leikur Card </Bold> - Controlled
                    spending with smart cards
                </BannerTitle>
                <BannerKeyInformation>
                    Provide your team members with the
                    powerful Leikur corporate card - and
                    get the best tools to control your
                    company’s spending
                </BannerKeyInformation>
            </ContentLeft>
            <ContentRight>
                <Graphic src={CardGraphic} />
                <BannerPoints>
                    <li>Physical and virtual corporate cards for everyone in
                        your team</li>
                    <li> Easily managed issuing and customised usage,
                        including limits, freezes and cancellations</li>
                    <li>Multi-currency transactions at favorable rates, and
                        automated expenses and receipts</li>
                    <li>  Provide your team members with the
                        powerful Leikur corporate card - and
                        get the best tools to control your
                        company’s spending.</li>
                </BannerPoints>

                <CardBtn>
                    Read More
                </CardBtn>
            </ContentRight>
            </Content>
        </Container>
        </LayoutMargin>
    )
}

export default CardsBanner

