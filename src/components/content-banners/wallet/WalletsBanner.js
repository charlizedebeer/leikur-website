import React from 'react'
import styled from "styled-components"
import Button from "../../ui-components/Button"
import WalletBg from "./wallet-bg.png"
import Phone from "./wallet-graphic.webp"
import LayoutMargin from "../layout.js"


const Container = styled.div
    `
      overflow: hidden;
      place-items: center;
      width: 100%;
      height: 90vh;
      background-color:  #4b88b1;
      background-image: url("${WalletBg}");
      background-size: cover;
      background-repeat:  no-repeat ;
      background-position: bottom;
      background-blend-mode: multiply;
      padding: 10vh 10vw;
      display: flex;
      flex-direction: column;
      @media screen and (max-width: 900px){
        padding: 2vh 5vw;
        height: fit-content;
        display: inline-block;
        flex-direction: row;}
    `

const Content = styled.div`
  height: content-box;
  margin: 5vh 0;
  place-items: center;
  @media screen and (max-width: 900px) {
    flex-direction: column;
    height: fit-content;
    min-height: 150vh;

  }

`

export const ContentLeft = styled.div`
  flex: 1 145%;
  place-content: center;
  max-width: 50%;
  min-height: 100%;
  display: block;
  flex-direction: row;
  float: left;
  @media screen and (max-width: 900px) {
    max-width: 100%;
    min-height: fit-content;
    margin: 0;
  }

`
export const BannerPoints  = styled.ul`
  list-style: square;
   padding: 5vh 0;
   font-size: calc(9px + 1vh);
  @media screen and (max-width: 1024px) {
padding: 2vh 0;
  }
  @media screen and (max-width: 900px) {
    max-width: 100%;
    margin: 0 10px;
  }`


export const Bold = styled.span`
font-weight: bold;
 
`
export const BannerTitle  = styled.div` 
 color: #fff;
  margin: 5vh 0;
 font-size: calc(1rem + 1vw);
  @media screen and (max-width: 1024px) {
margin: 2vh 0;
  }
  @media screen and (max-width: 900px) {
    padding: 5vh 0 2.5vh 0;
  }
 `
export const BannerKeyInformation = styled.div `
 font-size: calc(14px + 1vh);
 color: #fff;`


export const BannerButton = styled(Button)`
  background-color:#4b88b1;
  padding: 10px 15px;
  margin: 2vh 0 0 0;
  @media screen and (max-width: 900px) {
    margin-bottom: 10vh;
  }
`



export const ContentRight = styled.div`
  flex: 0 1 45%;
  height: 80vh;
  justify-content: right;
  align-content: space-around;

  align-self: stretch;
  @media screen and (max-width: 1024px) {
    height: fit-content;
    float: right;

  }
  @media screen and (max-width: 900px) {
    max-width: 100%;
    height: fit-content;
    flex: 100%
  }

`
export const Graphic = styled.img`
  width: 25rem;
  margin: -5vh 0 0 2vw;
  justify-self: right;
  height: 100%;
  aspect-ratio: 1089/1806;
  position: relative;
  @media screen and (max-width: 1024px) {
width: calc(20rem + 1vw);
    height: auto;
    float: right;
  }
  @media screen and (max-width: 900px) {
    width: 80vw;
    margin: -10vh 2vw;
    min-height: fit-content;
display: inline-block;
  }
`

const WalletBanner = () => {
    return(
        <LayoutMargin>
        < Container >
            <Content>
                <ContentLeft>
                    <BannerTitle>
                        <Bold>Flexible Wallets</Bold> - Controlled
                        spending with smart cards
                    </BannerTitle>
                    <BannerKeyInformation>
                        Provide your team members with the
                        powerful Leikur corporate card - and
                        get the best tools to control your
                        company’s spending
                        <BannerPoints>
                            <li>Physical and virtual corporate cards for everyone
                                in your team</li>
                            <li>Easily managed issuing and customised usage,
                                including limits, freezes and cancellations</li>
                            <li>Multi-currency transactions at favorable rates, and
                                automated expenses and receipts</li>
                        </BannerPoints>
                        <BannerButton>
                            Read More
                        </BannerButton>
                    </BannerKeyInformation>
                </ContentLeft>

                <ContentRight>
                    <Graphic src={Phone}/>
            </ContentRight>
            </Content>
        </Container>
        </LayoutMargin>
    )
}

export default WalletBanner