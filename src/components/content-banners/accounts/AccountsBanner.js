import React from 'react'
import styled from "styled-components"
import AccountsBg from "./accounts.png"
import AccountsGraphic from "./laptop-graphic.png"
import Button from "../../ui-components/Button";
import LayoutMargin from "../layout"
import Sepa from "./sepa-icn.png"
import Swift from "./swift-icn.png"



const Container = styled.div
    `
      overflow: hidden;
      place-items: center;
      width: 100%;
      height: 90vh;
      background-image: url("${AccountsBg}");
      background-color: rgba(204, 153, 68, 1);
      background-size: cover;
      background-repeat:  no-repeat ;
      background-position: bottom;
      background-blend-mode: multiply;
      padding: 10vh 10vw;
      display: flex;
      flex-direction: column;
      @media screen and (max-width: 1024px) {
        padding: 10vh 5vw;

      }
      @media screen and (max-width: 900px){
        padding: 2vh 5vw;
        height: fit-content;
        display: inline-block;
        flex-direction: row;}
    `

const Content = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-content: center;
  justify-content: space-between;
  @media screen and (max-width: 900px) {
flex-direction: column;
  }
`

export const ContentLeft = styled.div`
  flex: 0 1 45%;
  margin: 1vw;
  height: 80vh;
  width: fit-content;
  grid-template-rows: 0.9fr 1.6fr 0.5fr;
  grid-template-areas: 
    "title"
    "description"
    "icons";
  align-items: flex-end;

  @media screen and (max-width: 900px) {
    max-width: 100%;
    min-height: 50vh;
    height: fit-content;
    margin: 0;
  }
 

`
export const BannerPoints  = styled.ul`
  list-style: square;
  color: white;
  display: inline-block;
  align-self: center;
  grid-area: point;
  margin: 2vh 0;
  font-size: calc(8px + 1vh);
  width: 50ch;
  @media screen and (max-width: 1024px) {
    padding: 0 0 2vh 5vw;
    margin-top: -2vh;
    max-width: 45ch;

  }
  @media screen and (max-width: 900px) {
    max-width: 100%;
    margin: 0 10px;
  }
`


export const Bold = styled.span`
font-weight: bold;
 
`
export const BannerTitle  = styled.div` 
 color: #fff;
 font-size: calc(1rem + 1vw);
  align-self: end;
  padding-top: 10vh ;
  padding-bottom: 5vh;
  margin: 15px 0;
  @media screen and (max-width: 1024px) {
    margin: 2vh 0;
  }
  @media screen and (max-width: 900px) {
padding: 5vh 0 0 0;
  }
 `
export const BannerKeyInformation = styled.div `
 font-size: calc(12px + 1vh);
  align-self: start;
  max-width: 40ch;  
  word-break: break-word;
 color: #fff;`




export const ContentRight = styled.div`
  flex: 0 1 45%;
  height: 80vh;
  justify-self: right;
  align-content: space-around;
  align-self: stretch;
  @media screen and (max-width: 1024px) {
    height: fit-content;
    min-width: 40vw;
    align-self: center;
    align-content: center;
    float: right;
  }
 @media screen and (max-width: 900px) {
  max-width: 100%;
   height: fit-content;
   flex: 100%
 }

`
export const Graphic = styled.img`
  margin: 10vh 0 0 0;
  width: 27rem;
  aspect-ratio: 1239/835;
  position: relative;
  @media screen and (max-width:400px) {
    width: 35vw;
    margin: 2vh 0 0 0;
    height: auto;
    float: right;
  }

  @media screen and (max-width: 900px) {
    width: 80vw;
  }
  
`


 const AccIcn = styled.div`
display: inline-flex;
   margin-top: 20vh;
  flex-direction: row;
  flex-wrap: nowrap;
  grid-area: icons;
   padding: 10px;
   align-self: end;
   @media screen and (max-width: 1024px) {
margin-top: 10vh;
   }
   @media screen and (max-width: 900px) {
     margin-top: 2vh;
     
   }

`

const SepaIcn = styled.img`
  width: 9rem;
  margin: 0 10px;
  display: inline-block;
  @media screen and (max-width: 1024px) {
    width: 7rem;

  }
  @media screen and (max-width: 900px) {
width: 5rem;
  }
`

const SwiftIcn = styled.img`
  margin: 0 10px;
  display: inline-block;
  width: 4rem;
  @media screen and (max-width: 1024px) {
    width: 3rem;

  }  @media screen and (max-width: 900px) {
width: 2rem;
    margin: 0 10px 0 0 ;

  }
`



const BannerButton = styled(Button)`
  background-color: #D69630FF;
  padding: 10px 15px;
  margin: 5vh 0 2vh 0 ;
  @media screen and (max-width: 1024px) {
    margin: 1vh 4vw ;
  }

  @media screen and (max-width: 900px) {
margin-bottom: 10vh;
  }

`




const AccountsBanner = () => {
    return(
        <LayoutMargin>
        <Container>
            <Content>
            <ContentLeft>
                <BannerTitle>
                    <Bold> Leikur Account </Bold> - International transactions,
                    without hidden fees
                </BannerTitle>
                <BannerKeyInformation>
                    Take control of your finances and
                    administration with a Leikur business
                    account. Tailor-made feat-ures, an
                    intuitive interface and smart solution to
                    let your business grow.
                </BannerKeyInformation>
                <AccIcn>
                    <SwiftIcn src={Swift}/>
                    <SepaIcn src={Sepa}/>
                </AccIcn>
            </ContentLeft>
            <ContentRight>
                <Graphic src={AccountsGraphic}/>
                <BannerPoints>
                    <li>Manage your business with multiple accounts</li>
                    <li> Hassle-free, international and local payments -
                        free transfers between all Leikur accounts</li>
                    <li>   25+ currencies for true crossborder transactions,
                        at the best exchange rate. </li>
                </BannerPoints>
                <BannerButton>
                    Read More
                </BannerButton>
            </ContentRight>
            </Content>
        </Container>
        </LayoutMargin>
    )
}

export default AccountsBanner