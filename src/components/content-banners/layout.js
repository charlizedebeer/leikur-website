import React from 'react'
import styled from "styled-components";

 const LayoutMargin = styled.div`
  overflow-x: hidden;
  padding: 5vh 5vw;
  place-content: center;`

export default LayoutMargin
