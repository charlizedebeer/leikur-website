import React from 'react'
import styled from "styled-components"
import AboutBg from "./about-bg.png"
import LayoutMargin from "../layout.js"


const Container = styled.div
    `
      overflow: hidden;
      place-items: center;
      width: 100%;
      height: calc(35vw + 70vh);
      background-color:   rgba(115,84,105,1);
      background-image: url("${AboutBg}");
      background-size: cover;
      background-position: bottom;
      background-repeat:  no-repeat ;
      background-blend-mode: multiply;
      padding: 10vh 10vw;
      display: flex;
      margin-bottom: 5vh ;
      flex-direction: column;

      @media screen and (max-width: 900px){
        padding: 5vh 5vw;
        height: 100vh;
        flex-direction: row;}
      @media screen and (max-width: 400px){
        padding: 5vh 5vw;
        min-height: 200vh;

        flex-direction: row;}
    `

const Content = styled.div`
  height: content-box;
  place-items: center;
  flex: 1 145%;
  place-content: center;
  max-width: 100%;
  margin: 10vh 10vw;
  display: block;
  text-align: center;
  flex-direction: row;
  float: left;

  @media screen and (max-width: 1024px) {
    margin: 2vh 5vw 5vh 5vw ;
  }
  @media screen and (max-width: 900px) {
    flex: 100%;
    max-width: 100%;
  }
`


 const BannerPoints  = styled.text`
   padding: 5vh 0;
line-height: 150%;
   display: block;
   font-weight: normal;
   font-size: calc(11px + 1vh);
`


export const Bold = styled.span`
font-weight: bold;
 
`
export const BannerTitle  = styled.div` 
 color: #fff;
  display: block;
  margin: 5vh 0;
 font-size: calc(1rem + 1vw);
  
 `
export const BannerKeyInformation = styled.div `
 font-size: calc(14px + 1vh);
  display: block;
 color: #fff;`









const AboutBanner = () => {
    return(
        <LayoutMargin>
        < Container >
            <Content>
                    <BannerTitle>
                        <Bold>Leikur </Bold> - Who We Are
                    </BannerTitle>
                    <BannerKeyInformation>
                        Provide your team members with the
                        powerful Leikur corporate card - and
                        get the best tools to control your
                        company’s spending
                        <BannerPoints>
                        Leikur was created to address the needs of an industry in motion.
                            With traditional banks constanty changing and tightening their requirements,
                            and with harmonised international and national regulations impacting online
                            gaming operators, we sought to provide a banking service that adresses the specific,
                            essential ambitions and needs of the innovative gaming industry.
                        </BannerPoints>

                        <Bold>Enter Leikur.</Bold>

                        <BannerPoints>
                        The founders of Leikur have extensive, decades-long experience and know-how as innovators in the financial industry and leading-edge experience from the gaming and affiliate marketing sectors.
                        The Leikur team combines state-of-the art technology, banking, strategy & marketing experience and knowledge. Together we provide a banking service tailor-made for your purposes.
                        Finally.
                        </BannerPoints>
                    </BannerKeyInformation>
            </Content>
        </Container>
        </LayoutMargin>
    )
}

export default AboutBanner