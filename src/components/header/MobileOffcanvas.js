import React, {useState} from 'react'
import styled from "styled-components"
import Button from "../ui-components/Button"

import {StyledOffCanvas, Menu, Overlay}  from 'styled-off-canvas'

import {Menu as Icon} from "@styled-icons/material"

import { Close as CloseIcon } from '@styled-icons/remix-fill/Close'


const BurgerIcon = styled(Icon)    `
:&hover{
  color: #735469
}
  @media screen and (min-width: 1025px) {
    display: none ;
  }
`

const HamburgerWrapper = styled(StyledOffCanvas)`
z-index: 999;
  height: 30vw;
`

const NavList = styled.ul`
  list-style-type: none;
  margin: 0;
  display: contents;
  z-index:1000;
  overflow: visible;
  padding: 10px 5px;
`

const NavListItem = styled.li`
  display: block;
  margin: 0;
  padding: 0;
`

const NavItemMobile = styled.a`
  border-bottom: 1px solid #efefef;
  color: ${props => props.active ? 'rgba(218, 135, 196)' : '#333'};
  display: block;
  font-family: Helvetica, Arial, sans-serif;
  font-size: 16px;
  font-weight: 700;
  margin-left: 18px;
  padding: 18px 18px 18px 5px;
  text-decoration: none;
  text-transform: uppercase;
  transition: background 200ms ease-in-out;
  &:hover {
    color: rgba(218, 135, 196);
  }
`

const Close= ({ onClose }) => (
    <div
        css={{
            padding: '10px 10px 20px 10px',
            textAlign: 'right'
        }}
    >
        <CloseIcon
            onClick={onClose}
            size={36}
            css={{
                color: '#333',
                cursor: 'pointer',
                textAlign: 'center'
            }}
        />
    </div>
);

const MobileNav = () => {
    const [isOpen, setIsOpen] = useState(false)

    return (
        <HamburgerWrapper
            isOpen={isOpen}
            onClose={() => setIsOpen(false)}
        >
            <BurgerIcon
                size={48}
                css={{
                    background: '#2c2c2c',
                    borderRadius: '6px',
                    padding: '8px',
                    cursor: 'pointer'
                }}
                onClick={() => { setIsOpen((isOpen) => !isOpen) }}
            />

            <Menu>
                <NavList>
                    <NavListItem>
                        <Close onClick={() => setIsOpen(false)}/>
                    </NavListItem>
                    <NavListItem>
                        <NavItemMobile href='https://github.com/'>Home</NavItemMobile>
                    </NavListItem>
                    <NavListItem>
                        <NavItemMobile href='https://google.com/'>Cards </NavItemMobile>
                    </NavListItem>
                    <NavListItem>
                        <NavItemMobile href='https://google.com/'>Wallet </NavItemMobile>
                    </NavListItem>
                    <NavListItem>
                        <NavItemMobile href='https://google.com/'>About </NavItemMobile>
                    </NavListItem>
                    <NavListItem>
                        <Button blue> Open Account</Button>
                        <Button main> Login </Button>
                    </NavListItem>
                </NavList>
            </Menu>

            <Overlay />
        </HamburgerWrapper>
    )
}

export default MobileNav
