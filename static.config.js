// This file is used to configure:
// - static-site generation
// - Document shell (index.html)
import * as path from "path";

export default {
plugins: [
    [
      require.resolve('react-static-plugin-source-filesystem'),
      {
        location: path.resolve('./src/pages'),
      },
    ],
    require.resolve('react-static-plugin-reach-router'),
    require.resolve('react-static-plugin-sitemap'),
    require.resolve( "react-static-plugin-styled-components")
  ],

}
